<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    
    use App\Http\Controllers\App\ApplicationsController;
    use App\Http\Controllers\PagesController;
    
    Auth::routes();

Route::get('/', "PagesController@homepage");

Route::middleware('auth')->group(function(){
    Route::get('apply', "PagesController@apply")->name('apply');
    Route::post('apply', "PagesController@postApply")->name('apply');
    Route::get('situation', [PagesController::class, 'situation'])->name('situation');
    Route::post('applications', [ApplicationsController::class, 'store'])->name('applications.store');
    Route::get('applications', [ApplicationsController::class, 'index'])->middleware('application.handling')->name('applications.list');
});

