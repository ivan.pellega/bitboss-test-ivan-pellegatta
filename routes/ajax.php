<?php
    
    use App\Http\Controllers\App\ApplicationsController;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| AJAX Routes
|--------------------------------------------------------------------------
*/

/**
 * Routes per la gestione delle candidature
 */
Route::prefix('applications')
    ->as('applications.')
    ->group(function (Router $router) {
        $router->post('/accept_application/{applicationId}', [ApplicationsController::class, 'acceptApplication'])->name('accept_application');
        $router->post('/refuse_application/{applicationId}', [ApplicationsController::class, 'refuseApplication' ])->name('refuse_application');
    });
