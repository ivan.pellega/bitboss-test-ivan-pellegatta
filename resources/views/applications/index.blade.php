@extends('layouts.app')

@section('content')

    <applications inline-template>
        <div class="col-8 container mx-auto mt-5" style="height: 100vh !important">
            <h1 class="text-center my-5">Lista Candidature da valutare</h1>
            <table class="w-100">
                <thead>
                    <tr>
                        <th class="w-45">{{__("First Name")}}</th>
                        <th class="w-45">{{__("Last Name")}}</th>
                        <th class="px-1">{{__("Application Status")}}</th>
                        <th class="px-1">{{__("Approve")}}</th>
                        <th class="px-1">{{__("Refuse")}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(application, index) in items" :key="application.id">
                        <td class="w-45">@{{ application.first_name }}</td>
                        <td class="w-45">@{{ application.last_name }}</td>
                        <td class="px-1">
                            <input type="text" v-model="application.status" class="border-0 bg-light text-uppercase font-weight-bolder" />
                        </td>
                        <td v-if="application.status === 'pending'" class="px-1"><a href="javascript:;" @click="accept(application)">{{__("Approve")}}</a></td>
                        <td v-if="application.status === 'pending'" class="px-1"><a href="javascript:;" @click="refuse(application)">{{__("Refuse")}}</a></td>


                    </tr>
                </tbody>
            </table>

            <paginator :dataSet="dataSet" @changed="fetch"></paginator>
        </div>
    </applications>


@endsection