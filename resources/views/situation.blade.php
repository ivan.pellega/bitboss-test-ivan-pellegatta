@extends('layouts.app')

@section('content')
    <div class="container" style="height: 100vh !important;">
        @if(session('application_success'))
            <div class="alert alert-success">
                Candidatura inviata con successo
            </div>
        @endif
        <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h1>Candidatura</h1>
        </section>
        <section>
            <div class="row mx-auto">
                <div class="col-sm-12 d-flex justify-content-around">
                    <span>{{__("Your application is ") }}</span>
                    <div>
                        <span class=" px-2 text-uppercase font-weight-bold {{ Auth::user()->application->getStatus() }}">
                            {{ Auth::user()->application->getStatus() }}
                        </span>
                        <span class="px-2">
                            -> {{ Auth::user()->application->getUpdatedAt() }}
                        </span>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
