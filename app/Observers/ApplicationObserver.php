<?php

namespace App\Observers;

use App\Models\Application;
use App\Http\Services\NotificationService;

class ApplicationObserver
{
    private $notificationService;
    
    public function __construct(
        NotificationService $notificationService
    )
    {
        $this->notificationService = $notificationService;
    }
    /**
     * Handle the application "created" event.
     *
     * @param  Application  $application
     * @return void
     */
    public function created(Application $application)
    {
        $this->notificationService->sendCreateApplicationNotificationToUser($application);
        $this->notificationService->sendCreateApplicationNotificationToAdmin();
    }

    /**
     * Handle the application "updated" event.
     *
     * @param  Application  $application
     * @return void
     */
    public function updated(Application $application)
    {
        if ($application->isDirty('status'))
        {
            $this->notificationService->sendChangeStatusNotificationToUser($application);
        }
    }

    /**
     * Handle the application "deleted" event.
     *
     * @param  Application  $application
     * @return void
     */
    public function deleted(Application $application)
    {
        //
    }

    /**
     * Handle the application "restored" event.
     *
     * @param  Application  $application
     * @return void
     */
    public function restored(Application $application)
    {
        //
    }

    /**
     * Handle the application "force deleted" event.
     *
     * @param  \App\Application  $application
     * @return void
     */
    public function forceDeleted(Application $application)
    {
        //
    }
}
