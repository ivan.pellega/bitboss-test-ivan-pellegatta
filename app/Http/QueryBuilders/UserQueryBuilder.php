<?php
    
    namespace App\Http\QueryBuilders;
    
    use Illuminate\Database\Eloquent\Builder;

    class UserQueryBuilder extends Builder
    {
        public function whereIsAdmin(): self
        {
            return $this->where('is_admin', true);
        }
        
    }