<?php
    
    namespace App\Http\QueryBuilders;
    
    use Illuminate\Database\Eloquent\Builder;

    class ApplicationQueryBuilder extends Builder
    {
        public function whereUserId(int $userId): self
        {
            return $this->where('user_id', $userId);
        }
        
        public function whereId(int $id): self
        {
            return $this->where('id', $id);
        }
        
    }