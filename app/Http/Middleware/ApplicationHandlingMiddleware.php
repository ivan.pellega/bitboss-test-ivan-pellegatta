<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApplicationHandlingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! auth()->user()->isAdmin()) {
            return redirect('/')->withErrors([
                'You are not authorized to access this resource.',
            ]);
        }
        
        return $next($request);
    }
}
