<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\DTOs\ApplicationData;
use App\Http\Enum\ApplicationStatus;
use App\Http\Requests\ApplicationRequest;
use App\Http\Services\ApplicationCreateService;
use App\Http\Services\ApplicationRetrieverService;
use App\Http\Services\ApplicationUpdaterService;
use App\Http\Services\NotificationService;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationsController extends Controller
{
    
    private $applicationCreateService;
    private $applicationRetrieverService;
    private $applicationUpdaterService;
    private $notificationService;
    
    public function __construct(
        ApplicationCreateService $applicationCreateService,
        ApplicationRetrieverService $applicationRetrieverService,
        ApplicationUpdaterService $applicationUpdaterService,
        NotificationService $notificationService
        ){
        $this->applicationCreateService = $applicationCreateService;
        $this->applicationRetrieverService = $applicationRetrieverService;
        $this->applicationUpdaterService = $applicationUpdaterService;
        $this->notificationService = $notificationService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->expectsJson())
            return Application::paginate(20);

        return view('applications.index');
    }



    /**
     * @param int $applicationId
     */
    public function acceptApplication(int $applicationId)
    {
        try
        {
            $application = $this->applicationRetrieverService->getById($applicationId);
            $this->applicationUpdaterService->updateStatus($application, ApplicationStatus::ACCEPTED);

            return Application::paginate(20);
        } catch (\Exception $e)
        {
            return redirect()->back()->withErrors(['message' => $e->getMessage()]);
        }
    }
    
    /**
     * @param int $applicationId
     */
    public function refuseApplication(int $applicationId)
    {
        try
        {
            $application = $this->applicationRetrieverService->getById($applicationId);
            $this->applicationUpdaterService->updateStatus($application, ApplicationStatus::REFUSED);
    
            return Application::paginate(20);
        } catch (\Exception $e)
        {
            return redirect()->back()->withErrors(['message' => $e->getMessage()]);
        }
    }
    
    
    public function store(ApplicationRequest $applicationRequest)
    {
        try
        {
            $checkForUserApplication = $this->applicationRetrieverService->getByUserId(Auth::user()->id);

            if ($checkForUserApplication) {
                return redirect()->back()->withErrors(['message' => 'Hai già presentato una candidatura per questa posizione.']);
            }
            
            $this->applicationCreateService->create(new ApplicationData($applicationRequest->all()));
            
            return redirect()->route('situation')->with('application_success');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['message' => $e->getMessage()]);
        }
    }
}
