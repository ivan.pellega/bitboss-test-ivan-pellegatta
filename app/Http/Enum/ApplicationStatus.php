<?php
    
    namespace App\Http\Enum;
    
    class ApplicationStatus
    {
        
        const PENDING = 'pending';
        const ACCEPTED = 'accepted';
        const REFUSED = 'refused';
        
        public static function getValues(): array
        {
            return [
                self::PENDING,
                self::ACCEPTED,
                self::REFUSED,
            ];
        }
        
        
        public static function getValue(string $value): string
        {
            return self::getValues()[$value];
        }
    }