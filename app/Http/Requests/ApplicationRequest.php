<?php
    
    namespace App\Http\Requests;
    
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Support\Facades\Auth;

    /**
     * @property mixed first_name
     * @property mixed last_name
     * @property mixed email
     * @property mixed phone
     * @property mixed notes
     */
    class ApplicationRequest extends FormRequest
    {
        public function authorize(): bool
        {
            return Auth::check();
        }
        
        public function rules(): array
        {
            return [
                'first_name' => ['required', 'min:2', 'max:50'],
                'last_name' => ['required', 'min:2', 'max:50'],
                'email' => ['required', 'email', 'unique:applications'],
                'phone' => ['required', 'numeric', 'min:8'],
                'notes' => ['nullable'],
            ];
        }
        
        //custom messages
        public function messages(): array
        {
            return [
                'first_name.required' => 'First name is required',
                'first_name.min' => 'First name must be at least 2 characters',
                'first_name.max' => 'First name must be less than 50 characters',
                'last_name.required' => 'Last name is required',
                'last_name.min' => 'Last name must be at least 2 characters',
                'last_name.max' => 'Last name must be less than 50 characters',
                'email.required' => 'Email is required',
                'email.email' => 'Email must be a valid email address',
                'email.unique' => 'Email already exists',
                'phone.required' => 'Phone number is required',
                'phone.numeric' => 'Phone number must be a number',
                'phone.min' => 'Phone number must be at least 8 characters',
            ];
        }
    
        /**
         * @return mixed
         */
        public function getFirstName()
        {
            return $this->first_name;
        }
    
        /**
         * @return mixed
         */
        public function getLastName()
        {
            return $this->last_name;
        }
    
        /**
         * @return mixed
         */
        public function getEmail()
        {
            return $this->email;
        }
    
        /**
         * @return mixed
         */
        public function getPhone()
        {
            return $this->phone;
        }
    
        /**
         * @return mixed
         */
        public function getNotes(): ?string
        {
            if (!$this->notes) {
                return null;
            }
            return $this->notes;
        }
        
        
    }