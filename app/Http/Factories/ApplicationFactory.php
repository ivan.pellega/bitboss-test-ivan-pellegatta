<?php
    
    namespace App\Http\Factories;
    
    use App\Models\Application;

    class ApplicationFactory
    {
        /**
         * Class ApplicationFactory
         *
         * @package Factories
         *
         * @param int    $userId
         * @param string $firstName
         * @param string $lastName
         * @param string $email
         * @param string $phone
         * @param string $notes
         *
         *
         * @return Application
         */
        
        public function create(
            int    $userId,
            string $firstName,
            string $lastName,
            string $email,
            string $phone,
            ?string $notes
        ): Application
        {
            $application = new Application();
            
            $application->user_id = $userId;
            $application->first_name = $firstName;
            $application->last_name = $lastName;
            $application->email = $email;
            $application->phone = $phone;
            $application->notes = $notes;
            
            $application->save();
            
            return $application;
        }
        
        /**
         * @param int $userId
         *
         * @return Application|null
         */
        public function getByUserId(int $userId): ?Application
        {
            return Application::whereUserId($userId)->first();
        }
        
        public function getById(int $id): ?Application
        {
            return Application::whereId($id)->first();
        }
    }