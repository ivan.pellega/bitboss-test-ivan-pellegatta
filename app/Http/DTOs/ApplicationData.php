<?php
    
    namespace App\Http\DTOs;
    
    class ApplicationData
    {
        private $userId;
        private $firstName;
        private $lastName;
        private $email;
        private $phone;
        private $notes;
        
        public function __construct(
            array $data
        ){
            $this->firstName = $data['first_name'];
            $this->lastName = $data['last_name'];
            $this->email = $data['email'];
            $this->phone = $data['phone'];
            $this->notes = $data['notes'];
            
            return $this;
        }

        
        /**
         * @return string
         */
        public function getFirstName(): string
        {
            return $this->firstName;
        }
        
        /**
         * @return string
         */
        public function getLastName(): string
        {
            return $this->lastName;
        }
        
        /**
         * @return string
         */
        public function getEmail(): string
        {
            return $this->email;
        }
        
        /**
         * @return string
         */
        public function getPhone(): string
        {
            return $this->phone;
        }
        
        /**
         * @return string
         */
        public function getNotes(): ?string
        {
            if (!$this->notes) {
                return null;
            }
            return $this->notes;
        }
        
    }