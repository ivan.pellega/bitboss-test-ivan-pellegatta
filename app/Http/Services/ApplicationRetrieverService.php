<?php
    
    namespace App\Http\Services;
    
    use App\Http\Factories\ApplicationFactory;
    use App\Models\Application;

    class ApplicationRetrieverService
    {
        private $applicationFactory;
        
        public function __construct(ApplicationFactory $applicationFactory)
        {
            $this->applicationFactory = $applicationFactory;
        }
        
        public function getByUserId(int $userId): ?Application
        {
            return $this->applicationFactory->getByUserId($userId);
        }
        
        public function getById(int $id): ?Application
        {
            return $this->applicationFactory->getById($id);
        }
    }