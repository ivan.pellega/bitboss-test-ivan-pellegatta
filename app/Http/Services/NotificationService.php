<?php
    
    namespace App\Http\Services;
    
    use App\Actions\SendChangeStatusNotificationToUser;
    use App\Actions\SendCreateApplicationNotificationToAdmin;
    use App\Actions\SendCreateApplicationNotificationToUser;
    use App\Models\Application;
    use App\Models\User;
    use Illuminate\Support\Facades\Log;

    class NotificationService
    {
    
        private $sendChangeStatusNotificationToUser;
        private $sendCreateApplicationNotificationToUser;
        private $sendCreateApplicationNotificationToAdmin;
    
        public function __construct(
            SendChangeStatusNotificationToUser $sendChangeStatusNotificationToUser,
            SendCreateApplicationNotificationToUser $sendCreateApplicationNotificationToUser,
            SendCreateApplicationNotificationToAdmin $sendCreateApplicationNotificationToAdmin
        )
        {
            $this->sendChangeStatusNotificationToUser = $sendChangeStatusNotificationToUser;
            $this->sendCreateApplicationNotificationToUser = $sendCreateApplicationNotificationToUser;
            $this->sendCreateApplicationNotificationToAdmin = $sendCreateApplicationNotificationToAdmin;
    
        }
        public function sendChangeStatusNotificationToUser(Application $application)
        {
            try
            {
                $this->sendChangeStatusNotificationToUser->execute($application);
            }
            catch (\Exception $e)
            {
                Log::error($e->getMessage());
            }
        }
    
        public function sendCreateApplicationNotificationToUser(Application $application)
        {
            try
            {
                $this->sendCreateApplicationNotificationToUser->execute($application);
            }
            catch (\Exception $e)
            {
                Log::error($e->getMessage());
            }
        }
    
        public function sendCreateApplicationNotificationToAdmin()
        {
            try
            {
                $this->sendCreateApplicationNotificationToAdmin->execute();
            }
            catch (\Exception $e)
            {
                Log::error($e->getMessage());
            }
        }
    }