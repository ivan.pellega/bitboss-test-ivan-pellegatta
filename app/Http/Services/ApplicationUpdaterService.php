<?php
    
    namespace App\Http\Services;
    
    use App\Http\Factories\ApplicationFactory;
    use App\Models\Application;

    class ApplicationUpdaterService
    {
        
        public function updateStatus(Application $application, string $status): ?Application
        {
            $application->setStatus($status);
            $application->save();
            
            return $application;
        }
    }