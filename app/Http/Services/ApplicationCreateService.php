<?php
    
    namespace App\Http\Services;
    
    use App\Http\DTOs\ApplicationData;
    use App\Http\Factories\ApplicationFactory;
    use App\Models\Application;
    use Illuminate\Support\Facades\Auth;

    class ApplicationCreateService
    {
        private $applicationFactory;
    
        public function __construct(
            ApplicationFactory $applicationFactory
        ){
            $this->applicationFactory = $applicationFactory;
        }
        
        
        public function create(ApplicationData $applicationData): Application
        {
            $userId = Auth::user()->id;
            
            return $this->applicationFactory->create(
                
                $userId,
                $applicationData->getFirstName(),
                $applicationData->getLastName(),
                $applicationData->getEmail(),
                $applicationData->getPhone(),
                $applicationData->getNotes()
            );
        }
    }