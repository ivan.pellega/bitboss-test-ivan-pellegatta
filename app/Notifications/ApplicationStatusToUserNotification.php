<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApplicationStatusToUserNotification extends Notification
{
    use Queueable;
    
    /**
     * @var Application
     */
    private $application;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Abbiamo news sulla tua candidatura!')
                    ->line('Ehy ' . $this->application->getFirstName() . 'la tua candidatura presso BitBoss è stata aggiornata!')
                    ->line('Attualmente si trova in stato: ' . $this->application->status)
                    ->action('Ma ricorda che puoi sempre vedere lo stato cliccando qui', url('/situation'))
                    ->salutation('Grazie, BitBoss');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
