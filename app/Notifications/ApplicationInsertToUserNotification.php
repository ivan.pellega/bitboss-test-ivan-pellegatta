<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ApplicationInsertToUserNotification extends Notification
{
    use Queueable;
    
    /**
     * @var Application
     */
    private $application;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('La tua candidatura è stata ricevuta!')
            ->line('Ehy ' . $this->application->getFirstName() . 'la tua candidatura presso BitBoss è stata ricevuta!')
            ->line('Il prima possibile uno dei nostri HR la valuterà e deciderà se approvarla o meno')
            ->action('Ricorda che puoi sempre vedere lo stato cliccando qui', url('/situation'))
            ->salutation('Grazie, BitBoss');
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
