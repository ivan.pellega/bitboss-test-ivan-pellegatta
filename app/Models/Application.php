<?php

namespace App\Models;

use App\Http\QueryBuilders\ApplicationQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * @property int user_id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property numeric phone
 * @property string notes
 * @property string status
 */
class Application extends Model
{
    protected $guarded = [];
    
    protected $table = 'applications';
    
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'notes',
        'status'
    ];
    
    protected $defaultValues = [
        'status' => 'pending',
    ];
    
    public function newEloquentBuilder($query): ApplicationQueryBuilder
    {
        return new ApplicationQueryBuilder($query);
    }
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    
    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }
    
    /**
     * @param string $first_name
     */
    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }
    
    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }
    
    /**
     * @param string $last_name
     */
    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }
    
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    
    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
    
    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
    
    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }
    
    /**
     * @param string|null $notes
     */
    public function setNotes(?string $notes): void
    {
        $this->notes = $notes;
    }
    
    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
    
    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
    
    
    public function isPending(): bool
    {
        return $this->status === 'pending';
    }
    
    public function isApproved(): bool
    {
        return $this->status === 'approved';
    }
    
    public function isRefused(): bool
    {
        return $this->status === 'refused';
    }
    
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }
    
    
    
}
