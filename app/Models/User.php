<?php

namespace App\Models;

use App\Http\QueryBuilders\UserQueryBuilder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function newEloquentBuilder($query): UserQueryBuilder {
        return new UserQueryBuilder($query);
    }
    
    public function application(): HasOne {
        return $this->hasOne(Application::class);
    }
    
    public function isAdmin(): bool {
        return $this->is_admin;
    }
    
    public function isCandidate(): bool {
        return $this->application()->exists();
    }
}
