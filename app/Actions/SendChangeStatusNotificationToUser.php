<?php
    
    namespace App\Actions;
    
    use App\Models\Application;
    use App\Models\User;
    use App\Notifications\ApplicationStatusToUserNotification;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Notification;

    class SendChangeStatusNotificationToUser
    {
    
        public function execute(Application $application): bool
        {
            try
            {
                $user = User::where('id', $application->user_id)->first();
                
                $notification = new ApplicationStatusToUserNotification($application);
                
                Notification::send($user, $notification);
                
                return true;
            } catch (\Exception $e)
            {
                Log::error($e->getMessage());
                return false;
            }
        }
        
    }