<?php
    
    namespace App\Actions;
    
    use App\Models\Application;
    use App\Models\User;
    use App\Notifications\ApplicationInsertToUserNotification;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Notification;

    class SendCreateApplicationNotificationToUser
    {
    
        public function execute(Application $application): bool
        {
            try
            {
                $user = User::where('id', $application->user_id)->first();
                
                $notification = new ApplicationInsertToUserNotification($application);
                
                Notification::send($user, $notification);
                
                return true;
            } catch (\Exception $e)
            {
                Log::error($e->getMessage());
                return false;
            }
        }
        
    }