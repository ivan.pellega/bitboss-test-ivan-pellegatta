<?php
    
    namespace App\Actions;
    
    use App\Models\Application;
    use App\Models\User;
    use App\Notifications\ApplicationInsertToAdminsNotification;
    use App\Notifications\ApplicationStatusToUserNotification;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Notification;

    class SendCreateApplicationNotificationToAdmin
    {
    
        public function execute(): bool
        {
            try
            {
                $admins = User::whereIsAdmin()->get();
                
                $notification = new ApplicationInsertToAdminsNotification();
                
                Notification::send($admins, $notification);
                
                return true;
            } catch (\Exception $e)
            {
                Log::error($e->getMessage());
                return false;
            }
        }
        
    }