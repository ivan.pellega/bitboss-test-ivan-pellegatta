<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //seed users
        factory(App\Models\User::class, 10)->create();
        //seed applications
        factory(App\Models\Application::class, 10)->create();
    }
}
