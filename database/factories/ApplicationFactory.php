<?php
    
    use App\Models\User;
    use Faker\Generator as Faker;

$factory->define(/**
 * @param Faker $faker
 */ App\Models\Application::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
    ];
});
